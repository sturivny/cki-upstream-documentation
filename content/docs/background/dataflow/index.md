---
title: "Pipeline architecture"
description: >
  Diagram of the dataflow throughout the pipeline
---

You have a decent (albeit somewhat obsolete) high-level introduction in [under
the hood part 1] and [under the hood part 2]. See also the diagrams and notes
below.

## Triggers dataflow

The triggers are the way pipelines are started. There are several different
actors that can start them automatically, depending on where the code change
that needs to be tested is made: RHEL kernel merge requests, new commits in
upstream kernel trees, GitLab bots to test merge requests, etc.

The trigger code is defined in the [com/pipeline-trigger] and [com/cki-tools]
repositories, whereas the tree configurations and trigger variables used in the
triggers are defined in [com/pipeline-data] and [cee/pipeline-data].

[com/pipeline-trigger]: https://gitlab.com/cki-project/pipeline-trigger
[com/cki-tools]: https://gitlab.com/cki-project/cki-tools
[com/pipeline-data]: https://gitlab.com/cki-project/pipeline-data
[cee/pipeline-data]: https://gitlab.cee.redhat.com/cki-project/pipeline-data

![Triggers dataflow](pipeline-dataflow-triggers.png "Triggers dataflow")

## Results dataflow

![Results dataflow](pipeline-dataflow-results.png "Results dataflow")

[under the hood part 1]: ../../../../news/2019-04-08-under-the-hood-part-1.md
[under the hood part 2]: ../../../../news/2019-05-27-under-the-hood-part-2.md
