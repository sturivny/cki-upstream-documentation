---
title: Test Failure Notifications
linkTitle: Test Failure Notifications
description: How to be notified about a test failure.
weight: 25
---

## Test Failure Notifications (only applies to upstream builds)

**Test Maintainers:** If you maintain a test enabled in CKI pipeline, failures will
automatically be emailed to you.

**Non Test Maintainers:** If you are interested in monitoring failures, you are
welcome to subscribe to skt-results-master@ and filter on test failures, by
filtering on either 'Tests: FAILED' or Tests: PANICKED' within the body of the
email.

**Waiting for review:** Many of the test failure notifications are held for review
before releasing the reports to the internal/external kernel mailing lists.
This way it gives the test maintainers a time period to triage/review the
failure in case it's an infra/test issue. If a kernel bug is found, please
reply all with your finding so we can release the report. Otherwise the report
will never be sent. We are hoping to remove this stage soon, once as we improve
the way we report and mask known failures.
