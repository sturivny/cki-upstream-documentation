---
title: Onboarding a kernel tree
description: How to enable testing for a new kernel tree
linkTitle: Onboard to CKI
weight: 25
---

If you would like to enable testing for your kernel tree, we'll need to know
the following specifics:

* Which kernel tree and branch we should test ?
* Which tests should be run ?
* Which arches should we test ?
* Where should we send results ?

Please file a [PR] and we can help you fill in the remaining fields, or send
email to [cki-project@redhat.com] with the details above.

[cki-project@redhat.com]: mailto:cki-project@redhat.com
[PR]: https://gitlab.com/cki-project/pipeline-data
