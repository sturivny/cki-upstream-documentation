#!/bin/bash

set -euo pipefail

# Workaround for the Error When Loading Fake User Agent
# https://github.com/hellysmile/fake-useragent/issues/113
# Waiting for the MR https://github.com/hellysmile/fake-useragent/pull/121
sed -i 's/w3-table-all/ws-table-all/g' /usr/local/lib/python3.10/site-packages/fake_useragent/utils.py

# Read data and replace newlines with comas
excludes=$(cat check-links-*.txt | tr '\n' ',')

# Run the checker
urlchecker check . \
    --file-types "*.md" \
    --subfolder content/ \
    --exclude-pattern "${excludes}" \
    --timeout 60 \
    --retry-count 5 \
    --no-print
